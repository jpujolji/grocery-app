package com.codapps.groceryapp

import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.squareup.picasso.Picasso
import java.text.NumberFormat
import java.util.*

@BindingAdapter("formatPrice")
fun formatPrice(textView: TextView, price: Int?) {
    if (price != null) {
        val numberFormat = NumberFormat.getNumberInstance(Locale.getDefault())

        textView.text = "$ " + numberFormat.format(price)
    }

}

@BindingAdapter("loadImageFromUrl")
fun loadImageFromUrl(imageView: ImageView, image: String?) {
    if (image != null) {
        Picasso.get().load(image).into(imageView)
    }
}