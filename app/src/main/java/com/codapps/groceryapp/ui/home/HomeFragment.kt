package com.codapps.groceryapp.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.codapps.groceryapp.R
import com.codapps.groceryapp.data.models.Product
import com.codapps.groceryapp.databinding.FragmentHomeBinding


class HomeFragment : Fragment() {

    lateinit var binding: FragmentHomeBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)

        val productListAdapter = ProductListAdapter()

        binding.recyclerviewProducts.adapter = productListAdapter

        productListAdapter.submitList(getProductList())

        return binding.root
    }

    private fun getProductList(): MutableList<Product> {
        val products: MutableList<Product> = mutableListOf()

        products.add(
            Product(
                1,
                "Aguacate",
                "Pura gracita papa",
                5000,
                "https://exitocol.vtexassets.com/arquivos/ids/2374136-500-auto?width=500&height=auto&aspect=true"
            )
        )
        products.add(
            Product(
                2,
                "Banano",
                "Puro potasio papa",
                800,
                "https://exitocol.vtexassets.com/arquivos/ids/1720968-500-auto?width=500&height=auto&aspect=true"
            )
        )
        products.add(
            Product(
                3,
                "Pera",
                "Pera traida desde donde traen las mejores peras",
                1500,
                "https://exitocol.vtexassets.com/arquivos/ids/1721088-500-auto?width=500&height=auto&aspect=true"
            )
        )
        products.add(
            Product(
                4,
                "Mora",
                "Mora libra que contiene moras",
                3500,
                "https://exitocol.vtexassets.com/arquivos/ids/1027179-500-auto?width=500&height=auto&aspect=true"
            )
        )

        return products
    }

}