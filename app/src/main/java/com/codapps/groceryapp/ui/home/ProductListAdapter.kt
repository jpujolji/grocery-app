package com.codapps.groceryapp.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.codapps.groceryapp.data.models.Product
import com.codapps.groceryapp.databinding.ItemListProductBinding

class ProductListAdapter :
    ListAdapter<Product, RecyclerView.ViewHolder>(ProductDiffUtilCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ProductViewHolder(
            ItemListProductBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val product = getItem(position)
        (holder as ProductViewHolder).bind(product)
    }

    class ProductViewHolder(private val binding: ItemListProductBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Product) {
            binding.product = item

            binding.setClickListener {
                navigateToDetailProduct(item.id)
            }
        }

        private fun navigateToDetailProduct(idProduct: Int) {
            val direction = HomeFragmentDirections.navigateToDetailProductFragment(idProduct)

            binding.root.findNavController().navigate(direction)
        }

    }
}

/* DiffUtil.ItemCallback Clase que ayuda al momento de pintar la lista
 * a diferenciar el contenido que se va a mostrar
 */
private class ProductDiffUtilCallback : DiffUtil.ItemCallback<Product>() {
    override fun areItemsTheSame(oldItem: Product, newItem: Product): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Product, newItem: Product): Boolean {
        return oldItem.id == newItem.id
    }

}
