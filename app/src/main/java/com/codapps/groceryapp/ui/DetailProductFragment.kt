package com.codapps.groceryapp.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.codapps.groceryapp.R
import com.codapps.groceryapp.data.models.Product
import com.codapps.groceryapp.databinding.FragmentDetailProductBinding


class DetailProductFragment : Fragment() {

    private lateinit var binding: FragmentDetailProductBinding

    private val args: DetailProductFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_detail_product, container, false)

        binding.product = getProduct(args.idProduct)

        return binding.root
    }

    private fun getProduct(idProduct: Int): Product? {
        val products: MutableList<Product> = mutableListOf()

        products.add(
            Product(
                1,
                "Aguacate",
                "Pura gracita papa",
                5000,
                "https://exitocol.vtexassets.com/arquivos/ids/2374136-500-auto?width=500&height=auto&aspect=true"
            )
        )
        products.add(
            Product(
                2,
                "Banano",
                "Puro potasio papa",
                800,
                "https://exitocol.vtexassets.com/arquivos/ids/1720968-500-auto?width=500&height=auto&aspect=true"
            )
        )
        products.add(
            Product(
                3,
                "Pera",
                "Pera traida desde donde traen las mejores peras",
                1500,
                "https://exitocol.vtexassets.com/arquivos/ids/1721088-500-auto?width=500&height=auto&aspect=true"
            )
        )
        products.add(
            Product(
                4,
                "Mora",
                "Mora libra que contiene moras",
                3500,
                "https://exitocol.vtexassets.com/arquivos/ids/1027179-500-auto?width=500&height=auto&aspect=true"
            )
        )

        return products.find { product -> product.id == idProduct }
    }


}