package com.codapps.groceryapp.data.models

data class Product(
    val id: Int,
    val name: String,
    val description: String,
    val price: Int,
    val image: String
)